package com.mediasmartsoftware.moviecatalogueapi.model.tvshow;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TvShowGenreResponse {
    @SerializedName("genres")
    private List<TvShowGenres> genres;

    public List<TvShowGenres> getGenres() {
        return genres;
    }

    public void setGenres(List<TvShowGenres> genres) {
        this.genres = genres;
    }
}

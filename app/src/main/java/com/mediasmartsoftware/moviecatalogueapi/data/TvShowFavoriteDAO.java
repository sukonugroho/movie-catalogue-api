package com.mediasmartsoftware.moviecatalogueapi.data;


import com.mediasmartsoftware.moviecatalogueapi.model.favorite.FavoriteTvShow;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface TvShowFavoriteDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertFavoriteTvShow(FavoriteTvShow favoriteTvShow);

    @Query("SELECT * FROM tFavoriteTvShow")
    FavoriteTvShow[] selectAllFavoriteTvShow();

    @Query("SELECT * FROM tFavoriteTvShow WHERE id = :id LIMIT 1")
    FavoriteTvShow[] selectFavoriteTvShowDetail(int id);

    @Query("SELECT * FROM tFavoriteTvShow WHERE tv_show_id = :tv_show_id ")
    FavoriteTvShow[] selectFavoriteTvShowDetailByID(int tv_show_id);

    @Query("DELETE FROM tFavoriteTvShow WHERE tv_show_id = :tv_show_id ")
    int deleteFavoriteTvShowDetailByID(int tv_show_id);
}

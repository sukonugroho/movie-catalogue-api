package com.mediasmartsoftware.moviecatalogueapi.data.factory;

import android.content.Context;

import com.mediasmartsoftware.moviecatalogueapi.data.MovieFavoriteDAO;
import com.mediasmartsoftware.moviecatalogueapi.data.TvShowFavoriteDAO;
import com.mediasmartsoftware.moviecatalogueapi.model.favorite.FavoriteMovie;
import com.mediasmartsoftware.moviecatalogueapi.model.favorite.FavoriteTvShow;


import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

 @Database( entities =  {FavoriteMovie.class,
                         FavoriteTvShow.class} ,
            version = 1
           )
public abstract class DatabaseFavorite extends RoomDatabase {
     private static DatabaseFavorite INSTANCE;
     public abstract MovieFavoriteDAO movieFavoriteDAO();
     public abstract TvShowFavoriteDAO tvShowFavoriteDAO();

     public static DatabaseFavorite getAppDatabase(Context context) {
         if (INSTANCE == null) {
             INSTANCE = Room.databaseBuilder(context.getApplicationContext(), DatabaseFavorite.class, "db_favorite")
                            .allowMainThreadQueries()
                            .build();
         }
         return INSTANCE;
     }

     public static void destroyInstance() {
         INSTANCE = null;
     }
}



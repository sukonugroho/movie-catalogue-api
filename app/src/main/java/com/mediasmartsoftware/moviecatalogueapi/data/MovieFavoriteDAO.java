package com.mediasmartsoftware.moviecatalogueapi.data;

import com.mediasmartsoftware.moviecatalogueapi.model.favorite.FavoriteMovie;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface MovieFavoriteDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertFavoriteMovie(FavoriteMovie favoriteMovie);

    @Query("SELECT * FROM tFavoriteMovie")
    FavoriteMovie[] selectAllFavoriteMovie();

    @Query("SELECT * FROM tFavoriteMovie WHERE movie_id = :movie_id ")
    FavoriteMovie[] selectFavoriteMovieDetailByID(int movie_id);

    @Query("DELETE FROM tFavoriteMovie WHERE movie_id = :movie_id ")
    int deleteFavoriteMovieDetailByID (int movie_id);
}

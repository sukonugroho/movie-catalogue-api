package com.mediasmartsoftware.moviecatalogueapi.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import com.mediasmartsoftware.moviecatalogueapi.BuildConfig;

import java.util.Locale;

public class Config {
    public static final String IMAGE_URL_BASE_PATH = "https://image.tmdb.org/t/p/w500/";
    public static final String API_BASE_URL = "https://api.themoviedb.org/3/";
    public static final String API_KEY = BuildConfig.ApiKey ;

    public static String LANGUAGE_APPLICATION = "en-US";

    public void setLanguage(String language){
        if (language.equals("in")) {
            LANGUAGE_APPLICATION = "id-ID";
        } else{
            LANGUAGE_APPLICATION = "en-US";
        }
    }

    public void setLanguageOnActivity(Activity activity){
        Configuration config = activity.getBaseContext().getResources().getConfiguration();
        SharedPreferences mSettings = activity.getSharedPreferences("LANGUAGE", Context.MODE_PRIVATE);
        String language = mSettings.getString("LANG", config.locale.getLanguage());
        if (!"".equals(language) && !config.locale.getLanguage().equals(language)) {
            Locale locale = new Locale(language);
            Locale.setDefault(locale);
            config.locale = locale;

            setLanguage(locale.getLanguage());

            activity.getBaseContext().getResources().updateConfiguration(config, activity.getBaseContext().getResources().getDisplayMetrics());
            activity.recreate();
        }
    }
}

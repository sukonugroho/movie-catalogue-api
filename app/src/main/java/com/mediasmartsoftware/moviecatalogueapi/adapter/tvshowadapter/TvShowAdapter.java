package com.mediasmartsoftware.moviecatalogueapi.adapter.tvshowadapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowGenres;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowResults;
import com.mediasmartsoftware.moviecatalogueapi.util.Config;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TvShowAdapter extends RecyclerView.Adapter<TvShowAdapter.TvShowViewHolder> {
    private TvShowAdapter.OnItemClickCallback onItemClickCallback;
    private List<TvShowResults> listTvShowResults ;
    private List<TvShowGenres> listGenreTvShow;
    Context context;

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }


    public interface OnItemClickCallback {
        void onItemClicked(TvShowResults data);
    }


    public TvShowAdapter(Context context, List<TvShowResults> tvShowResults, List<TvShowGenres> tvShowGenres) {
        this.context = context;
        this.listTvShowResults = tvShowResults;
        this.listGenreTvShow = tvShowGenres;
    }

    @NonNull
    @Override
    public TvShowAdapter.TvShowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_movie, parent, false);
        return new TvShowAdapter.TvShowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TvShowAdapter.TvShowViewHolder holder, int position) {
        holder.tvTitle.setText(listTvShowResults.get(position).getName());
        //holder.tvGenre.setText(movieResults.get(position).getReleaseDate());
        holder.tvRate.setText(String.valueOf(listTvShowResults.get(position).getVoteAverage()));
        float rating = (float) (listTvShowResults.get(position).getVoteAverage() / 2);
        holder.ratingBar.setRating(rating);
        if (listGenreTvShow != null) {
            String genre = holder.getGenres(listTvShowResults.get(position).getGenreIds());
            holder.tvGenre.setText(genre);
        }
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(Config.IMAGE_URL_BASE_PATH + listTvShowResults.get(position).getPosterPath())
                .placeholder((R.color.white))
                .error(R.color.white)
                .into(holder.poster);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(listTvShowResults.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listTvShowResults.size();
    }

    public class TvShowViewHolder extends RecyclerView.ViewHolder {
        public ImageView poster;
        public TextView tvTitle;
        public TextView tvGenre;
        public TextView tvRate;
        public RatingBar ratingBar;


        public TvShowViewHolder(@NonNull View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.img_photo);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvGenre = itemView.findViewById(R.id.tv_genre);
            tvRate = itemView.findViewById(R.id.tv_rate);
            ratingBar = itemView.findViewById(R.id.ratingBar);
        }

        private String getGenres(List<Integer> genreIds) {
            List<String> movieGenres = new ArrayList<>();
            for (Integer genreId : genreIds) {
                for (TvShowGenres genre : listGenreTvShow) {
                    if (genre.getId() == genreId) {
                        movieGenres.add(genre.getName());
                        break;
                    }
                }
            }
            return TextUtils.join(", ", movieGenres);
        }
    }


}

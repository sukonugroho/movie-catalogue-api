package com.mediasmartsoftware.moviecatalogueapi.adapter.movieadapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieGenres;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieResults;
import com.mediasmartsoftware.moviecatalogueapi.util.Config;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {
    private MovieAdapter.OnItemClickCallback onItemClickCallback;
    private List<MovieResults> movieResults ;
    private List<MovieGenres> listGenreMovie;
    Context context;

    public interface OnItemClickCallback {
        void onItemClicked(MovieResults data);
    }

    public void setOnItemClickCallback(MovieAdapter.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public MovieAdapter(Context context,List<MovieResults> listMovieResults,List<MovieGenres> listGenreMovie){
        this.context = context;
        this.movieResults = listMovieResults;
        this.listGenreMovie = listGenreMovie;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_movie, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MovieViewHolder holder, int position) {
        holder.tvTitle.setText(movieResults.get(position).getTitle());
        //holder.tvGenre.setText(movieResults.get(position).getReleaseDate());
        holder.tvRate.setText(String.valueOf(movieResults.get(position).getVoteAverage()));
        float rating = (float) (movieResults.get(position).getVoteAverage() / 2);
        holder.ratingBar.setRating(rating);
        if (listGenreMovie != null) {
            String genre = holder.getGenres(movieResults.get(position).getGenreIds());
            holder.tvGenre.setText(genre);
        }
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(Config.IMAGE_URL_BASE_PATH + movieResults.get(position).getPosterPath())
                .placeholder((R.color.white))
                .error(R.color.white)
                .into(holder.poster);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(movieResults.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieResults.size();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder {
        public ImageView poster;
        public TextView tvTitle;
        public TextView tvGenre;
        public TextView tvRate;
        public RatingBar ratingBar;


        public MovieViewHolder(@NonNull View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.img_photo);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvGenre = itemView.findViewById(R.id.tv_genre);
            tvRate = itemView.findViewById(R.id.tv_rate);
            ratingBar = itemView.findViewById(R.id.ratingBar);
        }

        private String getGenres(List<Integer> genreIds) {
            List<String> movieGenres = new ArrayList<>();
            for (Integer genreId : genreIds) {
                for (MovieGenres genre : listGenreMovie) {
                    if (genre.getId() == genreId) {
                        movieGenres.add(genre.getName());
                        break;
                    }
                }
            }
            return TextUtils.join(", ", movieGenres);
        }
    }

}

package com.mediasmartsoftware.moviecatalogueapi.adapter.tvshowadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.adapter.movieadapter.FavoriteMovieAdapter;
import com.mediasmartsoftware.moviecatalogueapi.model.favorite.FavoriteMovie;
import com.mediasmartsoftware.moviecatalogueapi.model.favorite.FavoriteTvShow;
import com.mediasmartsoftware.moviecatalogueapi.util.Config;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FavoriteTvShowAdapter extends RecyclerView.Adapter<FavoriteTvShowAdapter.FavoriteTvShowViewHolder>{
    private FavoriteTvShowAdapter.OnItemClickCallback onItemClickCallback;

    private List<FavoriteTvShow> listFavoriteTvShows;
    Context context;

    public interface OnItemClickCallback {
        void onItemClicked(FavoriteTvShow data);
    }

    public void setOnItemClickCallback(FavoriteTvShowAdapter.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public FavoriteTvShowAdapter(ArrayList<FavoriteTvShow> listFavoriteTvShows, Context context) {
        this.context = context;
        this.listFavoriteTvShows = listFavoriteTvShows;
    }

    @NonNull
    @Override
    public FavoriteTvShowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_favorite_movie, parent, false);
        return new FavoriteTvShowAdapter.FavoriteTvShowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteTvShowViewHolder holder, int position) {
        holder.tvTitle.setText(listFavoriteTvShows.get(position).getTitle());
        holder.tvGenre.setText(listFavoriteTvShows.get(position).getGenre());
        holder.tvRate.setText(String.valueOf(listFavoriteTvShows.get(position).getVote_average()));
        holder.tvReleaseDate.setText(listFavoriteTvShows.get(position).first_air_date);
        float rating = (float) (listFavoriteTvShows.get(position).getVote_average() / 2);
        holder.ratingBar.setRating(rating);
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(Config.IMAGE_URL_BASE_PATH + listFavoriteTvShows.get(position).getPoster_path())
                .placeholder((R.color.white))
                .error(R.color.white)
                .into(holder.poster);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(listFavoriteTvShows.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listFavoriteTvShows.size();
    }

    public class FavoriteTvShowViewHolder extends RecyclerView.ViewHolder {
        public ImageView poster;
        public TextView tvTitle;
        public TextView tvGenre;
        public TextView tvRate;
        public TextView tvReleaseDate;
        public RatingBar ratingBar;
        public FavoriteTvShowViewHolder(@NonNull View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.img_photo);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvGenre = itemView.findViewById(R.id.tv_genre);
            tvRate = itemView.findViewById(R.id.tv_rate);
            tvReleaseDate = itemView.findViewById(R.id.tv_date);
            ratingBar = itemView.findViewById(R.id.ratingBar);
        }
    }
}

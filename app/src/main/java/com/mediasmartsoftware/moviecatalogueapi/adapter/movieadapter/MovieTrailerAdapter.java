package com.mediasmartsoftware.moviecatalogueapi.adapter.movieadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieTrailer;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MovieTrailerAdapter extends RecyclerView.Adapter<MovieTrailerAdapter.MovieHolder>{
    private MovieTrailerAdapter.OnItemClickCallback onItemClickCallback;
    private Context context;
    private List<MovieTrailer> listMovieTrailer;

    public MovieTrailerAdapter(Context context, List<MovieTrailer> listMovieTrailers) {
        this.context = context;
        this.listMovieTrailer = listMovieTrailers;
    }

    public void setListMovieTrailer(List<MovieTrailer> listMovieTrailer) {
        this.listMovieTrailer = listMovieTrailer;
    }
    public interface OnItemClickCallback {
        void onItemClicked(MovieTrailer movieTrailer);
    }
    public void setOnItemClickCallback(MovieTrailerAdapter.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }
    @NonNull
    @Override
    public MovieTrailerAdapter.MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_movie_trailer, parent, false);
        return new MovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieTrailerAdapter.MovieHolder holder, int position) {
        holder.tvName.setText(listMovieTrailer.get(position).getName());
        String urlPhoto = "https://img.youtube.com/vi/" + listMovieTrailer.get(position).getKey() + "/mqdefault.jpg";

        //Glide.with(context).load(urlPhoto).apply(new RequestOptions()).into(holder.imgTrailer);
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(urlPhoto)
                .placeholder((R.color.white))
                .error(R.color.white)
                .into(holder.imgTrailer);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(listMovieTrailer.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (listMovieTrailer != null) {
            if (listMovieTrailer.size() >= 5) {
                return 5;
            } else {
                return listMovieTrailer.size();
            }
        } else {
            return 0;
        }
    }

    class MovieHolder extends RecyclerView.ViewHolder {
        ImageView imgTrailer;
        TextView tvName;
        MovieHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            imgTrailer = itemView.findViewById(R.id.img_trailer);
        }
    }
}

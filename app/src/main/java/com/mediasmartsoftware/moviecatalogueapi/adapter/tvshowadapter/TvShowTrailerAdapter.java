package com.mediasmartsoftware.moviecatalogueapi.adapter.tvshowadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowTrailer;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TvShowTrailerAdapter extends RecyclerView.Adapter<TvShowTrailerAdapter.TvShowTrailerHolder>{
    private TvShowTrailerAdapter.OnItemClickCallback onItemClickCallback;
    private Context context;
    private List<TvShowTrailer> listTvShowTrailer;

    public TvShowTrailerAdapter(Context context, List<TvShowTrailer> listShowTrailer) {
        this.context = context;
        this.listTvShowTrailer = listShowTrailer;
    }

    public void setListTvShowTrailer(List<TvShowTrailer> listTvShowTrailer) {
        this.listTvShowTrailer = listTvShowTrailer;
    }
    public interface OnItemClickCallback {
        void onItemClicked(TvShowTrailer tvShowTrailer);
    }
    public void setOnItemClickCallback(TvShowTrailerAdapter.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }
    @NonNull
    @Override
    public TvShowTrailerAdapter.TvShowTrailerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_movie_trailer, parent, false);
        return new TvShowTrailerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TvShowTrailerAdapter.TvShowTrailerHolder holder, int position) {
        holder.tvName.setText(listTvShowTrailer.get(position).getName());
        String urlPhoto = "https://img.youtube.com/vi/" + listTvShowTrailer.get(position).getKey() + "/mqdefault.jpg";

        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(urlPhoto)
                .placeholder((R.color.white))
                .error(R.color.white)
                .into(holder.imgTrailer);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(listTvShowTrailer.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (listTvShowTrailer != null) {
            if (listTvShowTrailer.size() >= 5) {
                return 5;
            } else {
                return listTvShowTrailer.size();
            }
        } else {
            return 0;
        }
    }

    class TvShowTrailerHolder extends RecyclerView.ViewHolder {
        ImageView imgTrailer;
        TextView tvName;
        TvShowTrailerHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            imgTrailer = itemView.findViewById(R.id.img_trailer);
        }
    }
}

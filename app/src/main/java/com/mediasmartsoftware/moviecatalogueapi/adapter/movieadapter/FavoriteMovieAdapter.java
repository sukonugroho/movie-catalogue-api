package com.mediasmartsoftware.moviecatalogueapi.adapter.movieadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.model.favorite.FavoriteMovie;
import com.mediasmartsoftware.moviecatalogueapi.util.Config;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class FavoriteMovieAdapter extends RecyclerView.Adapter<FavoriteMovieAdapter.FavoriteMovieViewHolder>{
    private FavoriteMovieAdapter.OnItemClickCallback onItemClickCallback;
    private List<FavoriteMovie> listFavoriteMovies;
    Context context;

    public interface OnItemClickCallback {
        void onItemClicked(FavoriteMovie data);
    }

    public void setOnItemClickCallback(FavoriteMovieAdapter.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public FavoriteMovieAdapter(ArrayList<FavoriteMovie> listFavoriteMovie, Context context) {
        this.context = context;
        this.listFavoriteMovies = listFavoriteMovie;
    }

    @NonNull
    @Override
    public FavoriteMovieAdapter.FavoriteMovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_favorite_movie, parent, false);
        return new FavoriteMovieAdapter.FavoriteMovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteMovieAdapter.FavoriteMovieViewHolder holder, int position) {
        holder.tvTitle.setText(listFavoriteMovies.get(position).getTitle());
        holder.tvGenre.setText(listFavoriteMovies.get(position).getGenre());
        holder.tvRate.setText(String.valueOf(listFavoriteMovies.get(position).getVote_average()));
        holder.tvReleaseDate.setText(listFavoriteMovies.get(position).release_date);
        float rating = (float) (listFavoriteMovies.get(position).getVote_average() / 2);
        holder.ratingBar.setRating(rating);
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(Config.IMAGE_URL_BASE_PATH + listFavoriteMovies.get(position).getPoster_path())
                .placeholder((R.color.white))
                .error(R.color.white)
                .into(holder.poster);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(listFavoriteMovies.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listFavoriteMovies.size();
    }

    public class FavoriteMovieViewHolder extends RecyclerView.ViewHolder {
        public ImageView poster;
        public TextView tvTitle;
        public TextView tvGenre;
        public TextView tvRate;
        public TextView tvReleaseDate;
        public RatingBar ratingBar;

        public FavoriteMovieViewHolder(@NonNull View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.img_photo);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvGenre = itemView.findViewById(R.id.tv_genre);
            tvRate = itemView.findViewById(R.id.tv_rate);
            tvReleaseDate = itemView.findViewById(R.id.tv_date);
            ratingBar = itemView.findViewById(R.id.ratingBar);
        }
    }
}

package com.mediasmartsoftware.moviecatalogueapi.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.adapter.tvshowadapter.TvShowTrailerAdapter;
import com.mediasmartsoftware.moviecatalogueapi.data.factory.DatabaseFavorite;
import com.mediasmartsoftware.moviecatalogueapi.model.favorite.FavoriteTvShow;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowDetail;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowGenres;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowTrailer;
import com.mediasmartsoftware.moviecatalogueapi.util.Config;
import com.mediasmartsoftware.moviecatalogueapi.viewmodel.tvshowviewmodel.TvShowViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DetailTvShowActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String EXTRA_TV_SHOW_ID = "IDtvShow";
    Context context;
    private TvShowViewModel tvShowViewModel;
    private TvShowTrailerAdapter trailerAdapter;
    private RecyclerView rvVideos;
    private ImageView imgTrailer;
    private FrameLayout frameMovieTrailer;
    private ProgressBar progressBar;
    private StringBuilder genreBuilder = new StringBuilder();

    private TextView tvTitle;
    private TextView tvRelease;
    private TextView tvOverview;
    private TextView tvLanguage;
    private TextView tvRate;
    private TextView tvGenre;
    private Integer Id_TvShow;

    private RatingBar ratingBar;
    private ImageView imgPoster;
    private ImageView imgFavorite;
    private DatabaseFavorite db;
    private boolean isFavorite = false;
    private ArrayList<FavoriteTvShow> listFavoriteTvShows;
    private FavoriteTvShow favoriteTvShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tv_show);
        Config config = new Config();
        config.setLanguageOnActivity(this);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationIcon(R.drawable.arrows_left);
        toolbar.setTitle(R.string.detail_tv_show);
        toolbar.setSubtitle(R.string.detail_tv_show);
        toolbar.setNavigationOnClickListener(v -> finish());

        context = this  ;
        tvTitle = findViewById(R.id.tv_title);
        tvRelease = findViewById(R.id.tv_release_date);
        tvOverview = findViewById(R.id.tv_overview2);
        tvLanguage = findViewById(R.id.tv_language2);
        tvRate = findViewById(R.id.tv_rate);
        tvGenre = findViewById(R.id.tv_genre);
        ratingBar = findViewById(R.id.rating_bar);
        imgPoster = findViewById(R.id.img_poster);
        imgTrailer = findViewById(R.id.img_trailer);
        frameMovieTrailer = findViewById (R.id.linear_movie_trailer);
        imgFavorite = findViewById(R.id.img_favorite);


        Integer ID = getIntent().getIntExtra(EXTRA_TV_SHOW_ID,1);
        db = DatabaseFavorite.getAppDatabase(context);
        RefreshFavorite(ID);
        imgFavorite.setOnClickListener(this);

        progressBar = findViewById(R.id.progress_bar);
        showLoading(true);

        rvVideos = findViewById(R.id.rv_videos);
        rvVideos.setLayoutManager(new GridLayoutManager(this, 1));
        rvVideos.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvVideos.setHasFixedSize(true);

        tvShowViewModel = new ViewModelProvider(this, new ViewModelProvider.NewInstanceFactory()).get(TvShowViewModel.class);
        tvShowViewModel.setTVShowTrailer(ID);
        tvShowViewModel.setTVShoDetail(ID);

        getTvShowTrailer();
        getTvShowDetail();
    }
    private void RefreshFavorite(Integer Movie_ID){
        listFavoriteTvShows = new ArrayList<>();
        listFavoriteTvShows.addAll(Arrays.asList(db.tvShowFavoriteDAO().selectFavoriteTvShowDetailByID(Movie_ID)));
        if (listFavoriteTvShows.size() > 0) {
            isFavorite = true;
            Picasso.with(context)
                    .load(R.drawable.ic_favorite_pink_24dp)
                    .placeholder(R.drawable.ic_favorite_pink_24dp)
                    .into(imgFavorite);
        }else{
            isFavorite = false;
            Picasso.with(context)
                    .load(R.drawable.ic_favorite_border_pink_24dp)
                    .placeholder(R.drawable.ic_favorite_border_pink_24dp)
                    .into(imgFavorite);
        }
    }

    private void getTvShowDetail(){
        tvShowViewModel.getTvShowDetail().observe(this, new Observer<TvShowDetail>() {
            @Override
            public void onChanged(TvShowDetail tvShowDetail) {
                Id_TvShow= tvShowDetail.getId();
                tvTitle.setText(tvShowDetail.getName());
                tvRelease.setText(tvShowDetail.getFirstAirDate());
                tvRate.setText(String.valueOf(tvShowDetail.getVoteAverage()));
                tvLanguage.setText(String.valueOf(tvShowDetail.getOriginalLanguage()));
                if (!"".equals(tvShowDetail.getOverview())) {
                    tvOverview.setText(tvShowDetail.getOverview());
                }else{
                    tvOverview.setText(getString(R.string.overview_not_available));
                }

                float rating = (float) tvShowDetail.getVoteAverage() / 2;
                ratingBar.setRating(rating);

                for (int i = 0; i < tvShowDetail.getGenres().size(); i++) {
                    TvShowGenres tvShowGenres = tvShowDetail.getGenres().get(i);
                    if (i < tvShowDetail.getGenres().size() - 1) {
                        tvGenre.append(tvShowGenres.getName() + ", ");
                        genreBuilder.append(tvShowGenres.getName()).append(", ");
                    } else {
                        tvGenre.append(tvShowGenres.getName());
                        genreBuilder.append(tvShowGenres.getName());
                    }
                }
                String urlPhoto = Config.IMAGE_URL_BASE_PATH + tvShowDetail.getPosterPath();
                Picasso.Builder builder = new Picasso.Builder(context  );
                builder.downloader(new OkHttp3Downloader(context));
                builder.build().load(urlPhoto)
                        .placeholder((R.color.white))
                        .error(R.color.white)
                        .into(imgPoster);

                favoriteTvShow = new FavoriteTvShow();
                favoriteTvShow.setTitle(tvShowDetail.getName());
                favoriteTvShow.setTv_show_id(tvShowDetail.getId());
                favoriteTvShow.setFirst_air_date(tvShowDetail.getFirstAirDate());
                favoriteTvShow.setGenre(tvGenre.getText().toString());
                favoriteTvShow.setVote_average(tvShowDetail.getVoteAverage());
                favoriteTvShow.setPoster_path(tvShowDetail.getPosterPath());
                showLoading(false);
            }
        });
    }
    private void getTvShowTrailer(){
        tvShowViewModel.getTvShowTrailer().observe(this, new Observer<List<TvShowTrailer>>() {
            @Override
            public void onChanged(List<TvShowTrailer> tvShowTrailers) {
                if (tvShowTrailers != null) {
                    if (tvShowTrailers.size() != 0 ) {
                        trailerAdapter = new TvShowTrailerAdapter(context,tvShowTrailers );
                        trailerAdapter.notifyDataSetChanged();
                        rvVideos.setAdapter(trailerAdapter);
                        openYoutubeTrailer(tvShowTrailers.get(0).getKey());
                        showLoading(false);
                        trailerAdapter.setOnItemClickCallback(new TvShowTrailerAdapter.OnItemClickCallback() {
                            @Override
                            public void onItemClicked(TvShowTrailer data) {
                                openYoutubeMovieVideo(data.getKey());
                            }
                        });
                    }
                }
            }
        });
    }
    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
    private void openYoutubeMovieVideo(String keyMovie) {
        Intent appVideoIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("vnd.youtube:" + keyMovie));

        Intent webVideoIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + keyMovie));
        try {
            startActivity(appVideoIntent);
        } catch (ActivityNotFoundException ex) {
            startActivity(webVideoIntent);
        }
    }
    public void openYoutubeTrailer(String keyMovie) {
        String urlImage = "https://img.youtube.com/vi/" + keyMovie + "/hqdefault.jpg";
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(urlImage)
                .placeholder((R.color.white))
                .error(R.color.white)
                .into(imgTrailer);
        View view = LayoutInflater.from(this)
                .inflate(R.layout.layout_movie_trailer, frameMovieTrailer, false);

        Intent appTrailerIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("vnd.youtube:" + keyMovie));
        Intent webTrailerIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + keyMovie));

        view.setOnClickListener(v -> {
            try {
                startActivity(appTrailerIntent);
            } catch (ActivityNotFoundException ex) {
                startActivity(webTrailerIntent);
            }
        });
        frameMovieTrailer.addView(view);
    }


    @Override
    public void onClick(View v) {
        if (isFavorite ) {
            if (favoriteTvShow != null){
                deleteFavorite(Id_TvShow);
            }
        }else{
            if (favoriteTvShow != null){
                 insertFavorite(favoriteTvShow);
            }
        }
    }
    private void insertFavorite(final FavoriteTvShow favoriteTvShow){
        new AsyncTask<Void, Void, Long>(){
            @Override
            protected Long doInBackground(Void... voids) {
                long status = db.tvShowFavoriteDAO().insertFavoriteTvShow(favoriteTvShow);
                return status;
            }
            @Override
            protected void onPostExecute(Long status) {
                RefreshFavorite(Id_TvShow);
                if (status > 0 ) {
                    Toast.makeText(DetailTvShowActivity.this, tvTitle.getText() + " " + getString(R.string.msg_insert_sucses), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(DetailTvShowActivity.this, tvTitle.getText() + " " + getString(R.string.msg_insert_filed), Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }
    private void deleteFavorite(final Integer ID_TV){
        new AsyncTask<Void, Void, Long>(){
            @Override
            protected Long doInBackground(Void... voids) {
                long status = db.tvShowFavoriteDAO().deleteFavoriteTvShowDetailByID(ID_TV);
                return status;
            }
            @Override
            protected void onPostExecute(Long status) {
                RefreshFavorite(ID_TV);
                if (status == 1 ) {
                    Toast.makeText(DetailTvShowActivity.this, tvTitle.getText() + " " + getString(R.string.msg_delete_sucses), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(DetailTvShowActivity.this, tvTitle.getText() + " " + getString(R.string.msg_delete_filed), Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

}

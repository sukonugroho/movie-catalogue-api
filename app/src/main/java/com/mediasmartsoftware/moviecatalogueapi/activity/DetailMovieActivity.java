package com.mediasmartsoftware.moviecatalogueapi.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.adapter.movieadapter.MovieTrailerAdapter;
import com.mediasmartsoftware.moviecatalogueapi.data.factory.DatabaseFavorite;
import com.mediasmartsoftware.moviecatalogueapi.model.favorite.FavoriteMovie;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieDetail;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieGenres;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieTrailer;
import com.mediasmartsoftware.moviecatalogueapi.util.Config;
import com.mediasmartsoftware.moviecatalogueapi.viewmodel.movieviewmodel.MovieViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DetailMovieActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String EXTRA_MOVIE_ID = "IDMovie";
    private Context context;
    private MovieViewModel movieViewModel;
    private MovieTrailerAdapter trailerAdapter;
    private RecyclerView rvVideos;
    private ImageView imgTrailer;
    private FrameLayout frameMovieTrailer;
    private ProgressBar progressBar;
    private StringBuilder genreBuilder = new StringBuilder();

    private TextView tvTitle;
    private TextView tvRelease;
    private TextView tvOverview;
    private TextView tvDuration;
    private TextView tvRate;
    private TextView tvGenre;
    private Integer Id_Movie;

    private RatingBar ratingBar;
    private ImageView imgPoster;
    private ImageView imgFavorite;
    private DatabaseFavorite db;
    private boolean isFavorite = false;
    private ArrayList<FavoriteMovie> listFavoriteMovie;
    private FavoriteMovie favoriteMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);

        Config config = new Config();
        config.setLanguageOnActivity(this);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationIcon(R.drawable.arrows_left);
        toolbar.setTitle(R.string.detail_movie);
        toolbar.setSubtitle(R.string.detail_movie);
        toolbar.setNavigationOnClickListener(v -> finish());

        context = this  ;
        tvTitle = findViewById(R.id.tv_title);
        tvRelease = findViewById(R.id.tv_release_date);
        tvOverview = findViewById(R.id.tv_overview2);
        tvDuration = findViewById(R.id.tv_duration2);
        tvRate = findViewById(R.id.tv_rate);
        tvGenre = findViewById(R.id.tv_genre);
        ratingBar = findViewById(R.id.rating_bar);
        imgPoster = findViewById(R.id.img_poster);
        imgFavorite = findViewById(R.id.img_favorite);
        imgTrailer = findViewById(R.id.img_trailer);
        frameMovieTrailer = findViewById (R.id.linear_movie_trailer);


        Integer ID = getIntent().getIntExtra(EXTRA_MOVIE_ID,1);
        db = DatabaseFavorite.getAppDatabase(context);
        RefreshFavorite(ID);
        imgFavorite.setOnClickListener(this);

        progressBar = findViewById(R.id.progress_bar);
        showLoading(true);
        rvVideos = findViewById(R.id.rv_videos);
        rvVideos.setLayoutManager(new GridLayoutManager(this, 1));
        rvVideos.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        rvVideos.setHasFixedSize(true);
        movieViewModel = new ViewModelProvider(this, new ViewModelProvider.NewInstanceFactory()).get(MovieViewModel.class);
        movieViewModel.setMovieTrailer(ID);
        movieViewModel.setMovieDetail(ID);
        getMovieTrailer();
        getMovieDetail();

    }

    private void RefreshFavorite(Integer Movie_ID){
        listFavoriteMovie = new ArrayList<>();
        listFavoriteMovie.addAll(Arrays.asList(db.movieFavoriteDAO().selectFavoriteMovieDetailByID(Movie_ID)));
        if (listFavoriteMovie.size() > 0) {
            isFavorite = true;
            Picasso.with(context)
                    .load(R.drawable.ic_favorite_pink_24dp)
                    .placeholder(R.drawable.ic_favorite_pink_24dp)
                    .into(imgFavorite);
        }else{
            isFavorite = false;
            Picasso.with(context)
                    .load(R.drawable.ic_favorite_border_pink_24dp)
                    .placeholder(R.drawable.ic_favorite_border_pink_24dp)
                    .into(imgFavorite);
        }
    }

    private void getMovieDetail(){
        movieViewModel.getMovieDetail().observe(this, new Observer<MovieDetail>() {
            @Override
            public void onChanged(MovieDetail movieDetail) {
                Id_Movie = movieDetail.getId();
                tvTitle.setText(movieDetail.getTitle());
                tvRelease.setText(movieDetail.getReleaseDate());
                tvRate.setText(String.valueOf(movieDetail.getVoteAverage()));
                tvDuration.setText(String.valueOf(movieDetail.getRuntime()));
                if (!"".equals(movieDetail.getOverview())) {
                    tvOverview.setText(movieDetail.getOverview());
                }else{
                    tvOverview.setText(getString(R.string.overview_not_available));
                }

                float rating = (float) movieDetail.getVoteAverage() / 2;
                ratingBar.setRating(rating);

            for (int i = 0; i < movieDetail.getGenres().size(); i++) {
                MovieGenres movieGenres = movieDetail.getGenres().get(i);
                if (i < movieDetail.getGenres().size() - 1) {
                    tvGenre.append(movieGenres.getName() + ", ");
                    genreBuilder.append(movieGenres.getName()).append(", ");
                } else {
                    tvGenre.append(movieGenres.getName());
                    genreBuilder.append(movieGenres.getName());
                }
            }

            String urlPhoto = Config.IMAGE_URL_BASE_PATH + movieDetail.getPosterPath();
            Picasso.Builder builder = new Picasso.Builder(context  );
            builder.downloader(new OkHttp3Downloader(context));
            builder.build().load(urlPhoto)
                    .placeholder((R.color.white))
                    .error(R.color.white)
                    .into(imgPoster);
            favoriteMovie = new FavoriteMovie();
            favoriteMovie.setTitle(movieDetail.getTitle());
            favoriteMovie.setMovie_id(movieDetail.getId());
            favoriteMovie.setRelease_date(movieDetail.getReleaseDate());
            favoriteMovie.setGenre(tvGenre.getText().toString());
            favoriteMovie.setVote_average(movieDetail.getVoteAverage());
            favoriteMovie.setPoster_path(movieDetail.getPosterPath().toString());


            showLoading(false);
            }
        });
    }
    private void getMovieTrailer(){
        movieViewModel.getMovieTrailer().observe(this, new Observer<List<MovieTrailer>>() {
            @Override
            public void onChanged(List<MovieTrailer> movieTrailers) {
                if (movieTrailers != null) {
                    if (movieTrailers.size() != 0 ) {
                        trailerAdapter = new MovieTrailerAdapter(context,movieTrailers );
                        trailerAdapter.notifyDataSetChanged();
                        rvVideos.setAdapter(trailerAdapter);
                        openYoutubeTrailer(movieTrailers.get(0).getKey());
                        showLoading(false);
                        trailerAdapter.setOnItemClickCallback(new MovieTrailerAdapter.OnItemClickCallback() {
                            @Override
                            public void onItemClicked(MovieTrailer data) {
                                openYoutubeMovieVideo(data.getKey());
                            }
                        });
                    }
                }
            }
        });
    }
    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
    private void openYoutubeMovieVideo(String keyMovie) {
        Intent appVideoIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("vnd.youtube:" + keyMovie));

        Intent webVideoIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + keyMovie));
        try {
            startActivity(appVideoIntent);
        } catch (ActivityNotFoundException ex) {
            startActivity(webVideoIntent);
        }
    }
    private void openYoutubeTrailer(String keyMovie) {
        String urlImage = "https://img.youtube.com/vi/" + keyMovie + "/hqdefault.jpg";


        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(urlImage)
                .placeholder((R.color.white))
                .error(R.color.white)
                .into(imgTrailer);
        View view = LayoutInflater.from(this)
                .inflate(R.layout.layout_movie_trailer,frameMovieTrailer , false);

        Intent appTrailerIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("vnd.youtube:" + keyMovie));
        Intent webTrailerIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + keyMovie));

        view.setOnClickListener(v -> {
            try {
                startActivity(appTrailerIntent);
            } catch (ActivityNotFoundException ex) {
                startActivity(webTrailerIntent);
            }
        });
       frameMovieTrailer.addView(view);
    }

    @Override
    public void onClick(View v) {
        if (isFavorite ) {
            if (favoriteMovie != null){
                deleteFavorite(Id_Movie);
            }
        }else{
            if (favoriteMovie != null){
                insertFavorite(favoriteMovie);
            }
        }
    }

    private void insertFavorite(final FavoriteMovie favoriteMovie){
        new AsyncTask<Void, Void, Long>(){
            @Override
            protected Long doInBackground(Void... voids) {
                long status = db.movieFavoriteDAO().insertFavoriteMovie(favoriteMovie);
                return status;
            }
            @Override
            protected void onPostExecute(Long status) {
                RefreshFavorite(Id_Movie);
                if (status > 0 ) {
                    Toast.makeText(DetailMovieActivity.this,tvTitle.getText() + " " + getString(R.string.msg_insert_sucses), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(DetailMovieActivity.this, tvTitle.getText() + " " + getString(R.string.msg_insert_filed), Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }
    private void deleteFavorite(final Integer ID_Mov){
        new AsyncTask<Void, Void, Long>(){
            @Override
            protected Long doInBackground(Void... voids) {
                long status = db.movieFavoriteDAO().deleteFavoriteMovieDetailByID(ID_Mov);
                return status;
            }
            @Override
            protected void onPostExecute(Long status) {
                RefreshFavorite(Id_Movie);
                if (status == 1 ) {
                    Toast.makeText(DetailMovieActivity.this, tvTitle.getText() + " " + getString(R.string.msg_delete_sucses), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(DetailMovieActivity.this, tvTitle.getText() + " " + getString(R.string.msg_delete_filed), Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }
}

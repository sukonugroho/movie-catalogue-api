package com.mediasmartsoftware.moviecatalogueapi.activity;

import android.os.Bundle;

import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.util.Config;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Config config = new Config();
        config.setLanguageOnActivity(this);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationIcon(R.drawable.arrows_left);
        toolbar.setTitle(R.string.About);
        toolbar.setSubtitle(R.string.About);
        toolbar.setNavigationOnClickListener(v -> finish());
    }
}

package com.mediasmartsoftware.moviecatalogueapi.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mediasmartsoftware.moviecatalogueapi.MainActivity;
import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.util.Config;

import java.util.Locale;

public class SettingActivity extends AppCompatActivity {
    private Locale locale;

    private RadioGroup rgBahasa;
    private RadioButton rbEnglish;
    private RadioButton rbIndonesia;
    private Configuration config;
    private SharedPreferences mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        Config conf = new Config();
        conf.setLanguageOnActivity(this);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!= null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationIcon(R.drawable.arrows_left);
        toolbar.setTitle(R.string.title_setting);
        toolbar.setSubtitle(R.string.title_setting);
        toolbar.setNavigationOnClickListener(v -> finish());

        rgBahasa =  findViewById(R.id.rgBahasa);
        rbEnglish = findViewById(R.id.rbEnglish);
        rbIndonesia =  findViewById(R.id.rbIndonesia);

        config = this.getBaseContext().getResources().getConfiguration();
        mSettings = this.getSharedPreferences("LANGUAGE", MODE_PRIVATE);
        String language = mSettings.getString("LANG", config.locale.getLanguage());
        if (language.equals("in") ){
            rbIndonesia.setChecked(true);
        }else{
            rbEnglish.setChecked(true);
        }


        Button btnSave =  findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int selectedId = rgBahasa.getCheckedRadioButtonId();

                if (selectedId == rbEnglish.getId()){
                    setLanguage("en");
                } else if (selectedId == rbIndonesia.getId()){
                    setLanguage("in");
                    return;
                }
            }
        });
    }
    public  void setLanguage(String strLanguage){
        mSettings = this.getSharedPreferences("LANGUAGE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("LANG", strLanguage);
        editor.apply();

        Config configurationLang = new Config();
        configurationLang.setLanguage(strLanguage);

        config = this.getBaseContext().getResources().getConfiguration();
        locale = new Locale(strLanguage);
        Locale.setDefault(locale);
        config.locale = locale;
        this.getBaseContext().getResources().updateConfiguration(config, this.getBaseContext().getResources().getDisplayMetrics());
        this.recreate();

        Intent intentAkun = new Intent(this, MainActivity.class);
        startActivity(intentAkun);
    }
}

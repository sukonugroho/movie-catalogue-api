package com.mediasmartsoftware.moviecatalogueapi.service;

import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieDetail;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieGenreResponse;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieResponse;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieTrailerResponse;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowDetail;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowGenreResponse;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowResponse;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowTrailerResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetService {

    @GET("movie/popular")
    Call<MovieResponse> getMovieFromApi(@Query("api_key") String apiKey,@Query("language") String language);
    @GET("genre/movie/list")
    Call<MovieGenreResponse> getMovieGenreApi(@Query("api_key") String apiKey,@Query("language") String language);
    @GET("movie/{movie_id}")
    Call<MovieDetail> getMovieDetailApi(@Path("movie_id") int movieId, @Query("api_key") String apiKey,@Query("language") String language);
    @GET("movie/{movie_id}/videos")
    Call<MovieTrailerResponse> getMovieTrailerApi(@Path("movie_id") int movieId, @Query("api_key") String apiKey);



    @GET("tv/popular")
    Call<TvShowResponse> getTvPopular(@Query("api_key") String apiKey,@Query("language") String language);
    @GET("genre/tv/list")
    Call<TvShowGenreResponse> getTvGenre (@Query("api_key") String apiKey,@Query("language") String language);
    @GET("tv/{tv_id}/videos")
    Call<TvShowTrailerResponse> getTvTrailer(@Path("tv_id") int tvId, @Query("api_key") String apiKey);
    @GET("tv/{tv_id}")
    Call<TvShowDetail> getTvDetail(@Path("tv_id") int tvId, @Query("api_key") String apiKey,@Query("language") String language);
}

package com.mediasmartsoftware.moviecatalogueapi.viewmodel.tvshowviewmodel;

import android.util.Log;

import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowDetail;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowGenreResponse;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowGenres;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowResponse;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowResults;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowTrailer;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowTrailerResponse;
import com.mediasmartsoftware.moviecatalogueapi.service.ApiClient;
import com.mediasmartsoftware.moviecatalogueapi.service.GetService;
import com.mediasmartsoftware.moviecatalogueapi.util.Config;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvShowViewModel extends ViewModel {
    private GetService getService;

    public MutableLiveData<List<TvShowResults>> listTvShow = new MutableLiveData<>();
    public MutableLiveData<List<TvShowGenres>> listTVShowGenres = new MutableLiveData<>();
    public MutableLiveData<List<TvShowTrailer>> listTVShowTrailer = new MutableLiveData<>();
    public MutableLiveData<TvShowDetail> tvShowDetail = new MutableLiveData<>();

    public void setTvShowPopular() {
        final GetService service = ApiClient.getRetrofitInstance().create(GetService.class);
        Call<TvShowResponse> call = service.getTvPopular(Config.API_KEY,Config.LANGUAGE_APPLICATION);
        call.enqueue(new Callback<TvShowResponse>() {
            @Override
            public void onResponse(Call<TvShowResponse> call, Response<TvShowResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        listTvShow.setValue(response.body().getResults());
                    }
                }
                Log.d("Failure Get Movie 1", response.message());
            }
            @Override
            public void onFailure(Call<TvShowResponse> call, Throwable t) {
                Log.e("Something went wrong !", t.getMessage());
            }
        });
    }
    public void setTVShowGenres() {
        final GetService service = ApiClient.getRetrofitInstance().create(GetService.class);
        Call<TvShowGenreResponse> call = service.getTvGenre(Config.API_KEY,Config.LANGUAGE_APPLICATION);
        call.enqueue(new Callback<TvShowGenreResponse>() {
            @Override
            public void onResponse(Call<TvShowGenreResponse> call, Response<TvShowGenreResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        listTVShowGenres.setValue(response.body().getGenres());
                    }
                }
                Log.d("Failure Get Movie 1", response.message());
            }
            @Override
            public void onFailure(Call<TvShowGenreResponse> call, Throwable t) {
                Log.e("Something went wrong !", t.getMessage());
            }
        });
    }
    public void setTVShowTrailer(int movieId ) {
        getService = ApiClient.getRetrofitInstance().create(GetService.class);
        getService.getTvTrailer(movieId,Config.API_KEY).enqueue(new Callback<TvShowTrailerResponse>() {
            @Override
            public void onResponse(@NonNull Call<TvShowTrailerResponse> call, Response<TvShowTrailerResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        listTVShowTrailer.setValue(response.body().getResults());
                    }
                }
                Log.d("Failure Get Movie 1", response.message());
            }

            @Override
            public void onFailure(Call<TvShowTrailerResponse> call, Throwable t) {
                Log.e("Something went wrong !", t.getMessage());
            }
        });
    }
    public void setTVShoDetail(int movieId){
        final GetService service = ApiClient.getRetrofitInstance().create(GetService.class);
        Call<TvShowDetail> call = service.getTvDetail(movieId,Config.API_KEY,Config.LANGUAGE_APPLICATION);
        call.enqueue(new Callback<TvShowDetail>() {
            @Override
            public void onResponse(Call<TvShowDetail> call, Response<TvShowDetail> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        tvShowDetail.setValue(response.body());
                    }
                }
                Log.d("Failure Get Movie 1", response.message());
            }
            @Override
            public void onFailure(Call<TvShowDetail> call, Throwable t) {
                Log.e("Something went wrong !", t.getMessage());
            }
        });
    }

    public LiveData<List<TvShowResults>> getTvShowPopular() {
        return listTvShow ;
    }
    public LiveData<List<TvShowTrailer>> getTvShowTrailer() {
        return listTVShowTrailer ;
    }
    public LiveData<List<TvShowGenres>> getTvShowGenres() {
        return listTVShowGenres ;
    }
    public LiveData<TvShowDetail> getTvShowDetail() {
        return tvShowDetail ;
    }
}

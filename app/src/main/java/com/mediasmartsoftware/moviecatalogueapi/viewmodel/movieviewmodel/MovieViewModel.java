package com.mediasmartsoftware.moviecatalogueapi.viewmodel.movieviewmodel;

import android.util.Log;

import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieDetail;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieGenreResponse;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieGenres;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieResponse;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieResults;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieTrailer;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieTrailerResponse;
import com.mediasmartsoftware.moviecatalogueapi.service.ApiClient;
import com.mediasmartsoftware.moviecatalogueapi.service.GetService;
import com.mediasmartsoftware.moviecatalogueapi.util.Config;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieViewModel extends ViewModel {
    private GetService getService;

    public MutableLiveData<List<MovieResults>> listMovie = new MutableLiveData<>();
    public MutableLiveData<List<MovieGenres>> listMovieGenre = new MutableLiveData<>();

    public MutableLiveData<MovieDetail> movieDetail = new MutableLiveData<>();
    public MutableLiveData<List<MovieTrailer>> listMovieTrailer = new MutableLiveData<>();

    public void setMoviePopular() {
        final GetService service = ApiClient.getRetrofitInstance().create(GetService.class);
        Call<MovieResponse> call = service.getMovieFromApi(Config.API_KEY,Config.LANGUAGE_APPLICATION);
        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        listMovie.setValue(response.body().getResults());
                    }
                }
                Log.d("Failure Get Movie 1", response.message());
            }
            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                Log.e("Something went wrong !", t.getMessage());
            }
        });
    }

    public void setMovieGenre() {
        final GetService service = ApiClient.getRetrofitInstance().create(GetService.class);
        Call<MovieGenreResponse> call = service.getMovieGenreApi(Config.API_KEY,Config.LANGUAGE_APPLICATION);
        call.enqueue(new Callback<MovieGenreResponse>() {
            @Override
            public void onResponse(Call<MovieGenreResponse> call, Response<MovieGenreResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        listMovieGenre.setValue(response.body().getGenres());
                    }
                }
                Log.d("Failure Get Movie 1", response.message());
            }
            @Override
            public void onFailure(Call<MovieGenreResponse> call, Throwable t) {
                Log.e("Something went wrong !", t.getMessage());
            }
        });
    }

    public void setMovieDetail(int movieId) {
        final GetService service = ApiClient.getRetrofitInstance().create(GetService.class);
        Call<MovieDetail> call = service.getMovieDetailApi(movieId,Config.API_KEY,Config.LANGUAGE_APPLICATION);
        call.enqueue(new Callback<MovieDetail>() {
            @Override
            public void onResponse(Call<MovieDetail> call, Response<MovieDetail> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        movieDetail.setValue(response.body());
                    }
                }
                Log.d("Failure Get Movie 1", response.message());
            }
            @Override
            public void onFailure(Call<MovieDetail> call, Throwable t) {
                Log.e("Something went wrong !", t.getMessage());
            }
        });
    }

    public void setMovieTrailer(int movieId ) {
         getService = ApiClient.getRetrofitInstance().create(GetService.class);
         getService.getMovieTrailerApi(movieId,Config.API_KEY).enqueue(new Callback<MovieTrailerResponse>() {
            @Override
            public void onResponse(@NonNull Call<MovieTrailerResponse> call, Response<MovieTrailerResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        listMovieTrailer.setValue(response.body().getResults());
                    }
                }
                Log.d("Failure Get Movie 1", response.message());
            }

            @Override
            public void onFailure(Call<MovieTrailerResponse> call, Throwable t) {
                Log.e("Something went wrong !", t.getMessage());
            }
        });
    }

    public LiveData<List<MovieResults>> getMovieResult() {
        return listMovie ;
    }
    public LiveData<List<MovieGenres>> getMovieGenres() {
        return listMovieGenre;
    }
    public LiveData<MovieDetail> getMovieDetail(){
        return movieDetail;
    }
    public LiveData<List<MovieTrailer>> getMovieTrailer() {
        return listMovieTrailer;
    }


}

package com.mediasmartsoftware.moviecatalogueapi.fragment.favoritefragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.activity.DetailMovieActivity;
import com.mediasmartsoftware.moviecatalogueapi.adapter.movieadapter.FavoriteMovieAdapter;
import com.mediasmartsoftware.moviecatalogueapi.data.factory.DatabaseFavorite;
import com.mediasmartsoftware.moviecatalogueapi.model.favorite.FavoriteMovie;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieResults;

import java.util.ArrayList;
import java.util.Arrays;


public class FavoriteMovieFragment extends Fragment {
    private DatabaseFavorite db;
    private RecyclerView rv_favorite_movie;
    private FavoriteMovieAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<FavoriteMovie> listFavoriteMovie;
    private Context context;

    public FavoriteMovieFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView =  inflater.inflate(R.layout.fragment_favorite_movie, container, false);

        db = DatabaseFavorite.getAppDatabase(context);
        rv_favorite_movie = rootView.findViewById(R.id.rv_favorite_movie);
        rv_favorite_movie.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        rv_favorite_movie.setLayoutManager(layoutManager);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        listFavoriteMovie = new ArrayList<>();
        listFavoriteMovie.addAll(Arrays.asList(db.movieFavoriteDAO().selectAllFavoriteMovie()));
        adapter = new FavoriteMovieAdapter(listFavoriteMovie, context);
        rv_favorite_movie.setAdapter(adapter);
        adapter.setOnItemClickCallback(new FavoriteMovieAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(FavoriteMovie data) {
                showSelectedMovie(data);
            }
        });
    }

    private void showSelectedMovie(FavoriteMovie favoriteMovie) {
        Intent intent = new Intent(context, DetailMovieActivity.class);
        MovieResults movieResults = new MovieResults();
        movieResults.setId(favoriteMovie.getMovie_id());
        Integer IDMovie = favoriteMovie.getMovie_id();
        intent.putExtra(DetailMovieActivity.EXTRA_MOVIE_ID, IDMovie);
        startActivity(intent);
    }

}

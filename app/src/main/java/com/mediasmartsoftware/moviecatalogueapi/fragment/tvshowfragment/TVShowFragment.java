package com.mediasmartsoftware.moviecatalogueapi.fragment.tvshowfragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.activity.DetailTvShowActivity;
import com.mediasmartsoftware.moviecatalogueapi.adapter.tvshowadapter.TvShowAdapter;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowGenres;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowResults;
import com.mediasmartsoftware.moviecatalogueapi.viewmodel.tvshowviewmodel.TvShowViewModel;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class TVShowFragment extends Fragment {
    private TvShowAdapter adapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private Context context;
    private List<TvShowGenres> tvShowGenres;
    private TvShowViewModel tvShowViewModel;


    public TVShowFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView =  inflater.inflate(R.layout.fragment_movie, container, false);
        progressBar = rootView.findViewById(R.id.progress_bar);
        showLoading(true);

        recyclerView = rootView.findViewById(R.id.rv_movie);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 3));
        recyclerView.setHasFixedSize(true);

        tvShowViewModel = new ViewModelProvider(this,new ViewModelProvider.NewInstanceFactory()).get(TvShowViewModel.class);
        tvShowViewModel.setTvShowPopular();
        tvShowViewModel.setTVShowGenres();
        tvShowViewModel.setTvShowPopular();

        getTvShowPopular();
        getTvShowGenre();
        return  rootView;
    }

    private void getTvShowPopular(){
        tvShowViewModel.getTvShowPopular().observe((LifecycleOwner) context, new Observer<List<TvShowResults>>() {
            @Override
            public void onChanged(List<TvShowResults> tvShowResults) {
                if (tvShowResults != null) {
                    adapter = new TvShowAdapter(context,tvShowResults,tvShowGenres);
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                    showLoading(false);
                    adapter.setOnItemClickCallback(new TvShowAdapter.OnItemClickCallback() {
                        @Override
                        public void onItemClicked(TvShowResults data) {
                            showSelectedTvShow(data);
                        }
                    });
                }
            }
        });
    }
    private void showSelectedTvShow(TvShowResults tvShowResults) {
        Intent intent = new Intent(context, DetailTvShowActivity.class);
        TvShowResults tvShowResults1 = new TvShowResults();
        tvShowResults1.setId(tvShowResults.getId());
        Integer IDTvShow = tvShowResults.getId();
        intent.putExtra(DetailTvShowActivity.EXTRA_TV_SHOW_ID, IDTvShow);
        startActivity(intent);
    }
    private void  getTvShowGenre(){
        tvShowViewModel.getTvShowGenres().observe(getViewLifecycleOwner(), new Observer<List<TvShowGenres>>() {
            @Override
            public void onChanged(List<TvShowGenres> dataTvShowGenres) {
                tvShowGenres = dataTvShowGenres;
            }
        });
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

}

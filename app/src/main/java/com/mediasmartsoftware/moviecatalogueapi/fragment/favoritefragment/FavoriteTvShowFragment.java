package com.mediasmartsoftware.moviecatalogueapi.fragment.favoritefragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.activity.DetailTvShowActivity;
import com.mediasmartsoftware.moviecatalogueapi.adapter.tvshowadapter.FavoriteTvShowAdapter;
import com.mediasmartsoftware.moviecatalogueapi.data.factory.DatabaseFavorite;
import com.mediasmartsoftware.moviecatalogueapi.model.favorite.FavoriteTvShow;
import com.mediasmartsoftware.moviecatalogueapi.model.tvshow.TvShowResults;

import java.util.ArrayList;
import java.util.Arrays;


public class FavoriteTvShowFragment extends Fragment {
    private DatabaseFavorite db;
    private RecyclerView rv_favorite_tv_show;
    private FavoriteTvShowAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<FavoriteTvShow> listFavoriteTvShows;
    private Context context;

    public FavoriteTvShowFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView =  inflater.inflate(R.layout.fragment_favorite_tv_show, container, false);

        db = DatabaseFavorite.getAppDatabase(context);
        rv_favorite_tv_show = rootView.findViewById(R.id.rv_favorite_tv_show);
        rv_favorite_tv_show.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        rv_favorite_tv_show.setLayoutManager(layoutManager);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        listFavoriteTvShows = new ArrayList<>();
        listFavoriteTvShows.addAll(Arrays.asList(db.tvShowFavoriteDAO().selectAllFavoriteTvShow()));
        adapter = new FavoriteTvShowAdapter(listFavoriteTvShows, context);
        rv_favorite_tv_show.setAdapter(adapter);
        adapter.setOnItemClickCallback(new FavoriteTvShowAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(FavoriteTvShow data) {
                showSelectedTvShow(data);
            }
        });
    }

    private void showSelectedTvShow(FavoriteTvShow favoriteTvShow) {
        Intent intent = new Intent(context, DetailTvShowActivity.class);
        TvShowResults tvShowResults = new TvShowResults();
        tvShowResults.setId(favoriteTvShow.getTv_show_id());
        Integer IDMovie = favoriteTvShow.getTv_show_id();
        intent.putExtra(DetailTvShowActivity.EXTRA_TV_SHOW_ID, IDMovie);
        startActivity(intent);



        /*Intent intent = new Intent(context, DetailTvShowActivity.class);
        TvShowResults tvShowResults1 = new TvShowResults();
        tvShowResults1.setId(tvShowResults.getId());
        Integer IDTvShow = tvShowResults.getId();
        intent.putExtra(DetailTvShowActivity.EXTRA_TV_SHOW_ID, IDTvShow);
        startActivity(intent);*/
    }

}

package com.mediasmartsoftware.moviecatalogueapi.fragment.moviefragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mediasmartsoftware.moviecatalogueapi.R;
import com.mediasmartsoftware.moviecatalogueapi.activity.DetailMovieActivity;
import com.mediasmartsoftware.moviecatalogueapi.adapter.movieadapter.MovieAdapter;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieGenres;
import com.mediasmartsoftware.moviecatalogueapi.model.movie.MovieResults;
import com.mediasmartsoftware.moviecatalogueapi.viewmodel.movieviewmodel.MovieViewModel;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends Fragment {
    private MovieAdapter adapter;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private Context context;
    private List<MovieGenres> movieGenres;
    private MovieViewModel movieViewModel;

    public MovieFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView =  inflater.inflate(R.layout.fragment_movie, container, false);
        progressBar = rootView.findViewById(R.id.progress_bar);
        showLoading(true);

        recyclerView = rootView.findViewById(R.id.rv_movie);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 3));
        recyclerView.setHasFixedSize(true);

        movieViewModel = new ViewModelProvider(this, new ViewModelProvider.NewInstanceFactory()).get(MovieViewModel.class);
        movieViewModel.setMoviePopular();
        movieViewModel.setMovieGenre();
        movieViewModel.setMoviePopular();
        getMovieResult();
        getMovieGenre();
        getMovieResult();

        return  rootView;
    }

    private void getMovieResult(){
        movieViewModel.getMovieResult().observe((LifecycleOwner) context, new Observer<List<MovieResults>>() {
            @Override
            public void onChanged(List<MovieResults> movieResults) {
                if (movieResults != null) {
                    adapter = new MovieAdapter(context,movieResults,movieGenres);
                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);
                    showLoading(false);
                    adapter.setOnItemClickCallback(new MovieAdapter.OnItemClickCallback() {
                        @Override
                        public void onItemClicked(MovieResults data) {
                            showSelectedMovie(data);
                        }
                    });

                }
            }
        });
    }

    private void  getMovieGenre(){
        movieViewModel.getMovieGenres().observe(getViewLifecycleOwner(), new Observer<List<MovieGenres>>() {
            @Override
            public void onChanged(List<MovieGenres> movieGenresList) {
                movieGenres = movieGenresList;
            }
        });
    }

    private void showSelectedMovie(MovieResults movieCatalogue) {
        Intent intent = new Intent(context, DetailMovieActivity.class);
        MovieResults movieResults = new MovieResults();
        movieResults.setId(movieCatalogue.getId());
        Integer IDMovie = movieCatalogue.getId();
        intent.putExtra(DetailMovieActivity.EXTRA_MOVIE_ID, IDMovie);
        startActivity(intent);
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }


}
